package com.example.rx_sample_app;

import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.rx_sample_app.model.User;
import com.example.rx_sample_app.rx.RxDataManager;
import io.reactivex.MaybeObserver;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private static final String TAG = MainActivity.class.getCanonicalName();
  private static final String TAG_TEST = "TEST_TAG";
  private static final String TAG_DISPOSE = "DISPOSE_TAG";

  private Disposable disposable;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    //createExample();
    //justExample();

    //singleExample();

    //maybeExample();
    //maybeEmptyExample();

    //threadingExampleMainThread();
    //threadingExampleMixedThreads();

    //mapExample();
    //filterExample();
    //zipExample();
  }

  @Override
  protected void onDestroy() {
    Log.d(TAG_DISPOSE, "onDestroy()");
    if (disposable != null && !disposable.isDisposed()) {
      Log.d(TAG_DISPOSE, "Disposing disposable...");
      disposable.dispose();
    } else {
      Log.d(TAG_DISPOSE, "Disposable is NULL or already disposed!");
    }
    super.onDestroy();
  }

  private void createExample() {
    RxDataManager.getInstance().createExampleObservable()
        .subscribe(new Observer<User>() {
          @Override
          public void onSubscribe(@NonNull Disposable d) {
            disposable = d;
            Log.d(TAG_TEST, "createExample() - subscribed...");
          }

          @Override
          public void onNext(@NonNull User user) {
            Log.d(TAG_TEST, "Next user: " + user.toString());
          }

          @Override
          public void onError(@NonNull Throwable e) {
            Log.e(TAG_TEST, "createExample() - An error occurred: " + e.getMessage());
          }

          @Override
          public void onComplete() {
            Log.d(TAG_TEST, "createExample() - Completed!");
          }
        });
  }

  private void justExample() {
    RxDataManager.getInstance().justExampleObservable()
        .subscribe(new Observer<Integer>() {
          @Override
          public void onSubscribe(Disposable d) {
            disposable = d;
            Log.d(TAG_TEST, "justExample() - subscribed...");
          }

          @Override
          public void onNext(Integer integer) {
            Log.d(TAG_TEST, "Next item: " + integer);
          }

          @Override
          public void onError(Throwable e) {
            Log.e(TAG_TEST, "justExample() - An error occurred: " + e.getMessage());
          }

          @Override
          public void onComplete() {
            Log.d(TAG_TEST, "justExample() - Completed!");
          }
        });
  }

  private void singleExample() {
    RxDataManager.getInstance().singleExample()
        .subscribe(new SingleObserver<String>() {
          @Override
          public void onSubscribe(@NonNull Disposable d) {
            disposable = d;
            Log.d(TAG_TEST, "singleExample() - subscribed...");
          }

          @Override
          public void onSuccess(@NonNull String s) {
            Log.d(TAG_TEST, "singleExample() - The result: " + s);
          }

          @Override
          public void onError(@NonNull Throwable e) {
            Log.e(TAG_TEST, "singleExample() - An error occurred: " + e.getMessage());
          }
        });
  }

  private void maybeExample() {
    RxDataManager.getInstance().maybeExample()
        .subscribe(new MaybeObserver<String>() {
          @Override
          public void onSubscribe(@NonNull Disposable d) {
            disposable = d;
            Log.d(TAG_TEST, "maybeExample() - subscribed...");
          }

          @Override
          public void onSuccess(@NonNull String s) {
            Log.d(TAG_TEST, "maybeExample() - The result: " + s);
          }

          @Override
          public void onError(@NonNull Throwable e) {
            Log.e(TAG_TEST, "maybeExample() - An error occurred: " + e.getMessage());
          }

          @Override
          public void onComplete() {
            Log.d(TAG_TEST, "maybeExample() - Completed!");
          }
        });
  }

  private void maybeEmptyExample() {
    RxDataManager.getInstance().emptyMaybeExample()
        .subscribe(new MaybeObserver<String>() {
          @Override
          public void onSubscribe(@NonNull Disposable d) {
            disposable = d;
            Log.d(TAG_TEST, "maybeEmptyExample() - subscribed...");
          }

          @Override
          public void onSuccess(@NonNull String s) {
            Log.d(TAG_TEST, "maybeEmptyExample() - The result: " + s);
          }

          @Override
          public void onError(@NonNull Throwable e) {
            Log.e(TAG_TEST, "maybeEmptyExample() - An error occurred: " + e.getMessage());
          }

          @Override
          public void onComplete() {
            Log.d(TAG_TEST, "maybeEmptyExample() - Completed!");
          }
        });
  }

  private void threadingExampleMixedThreads() {
    disposable = RxDataManager.getInstance().threadingExampleMixedThreads()
        .subscribe(length -> Log.d(TAG_TEST, "Result length = " + length));
  }

  private void threadingExampleMainThread() {
    disposable = RxDataManager.getInstance().threadingExampleMainThread()
        .subscribe(length -> Log.d(TAG_TEST, "Result length = " + length));
  }

  private void mapExample() {
    RxDataManager.getInstance().mapExampleObservable()
        .subscribe(new Observer<String>() {
          @Override
          public void onSubscribe(Disposable d) {
            disposable = d;
            Log.d(TAG_TEST, "mapExample() - subscribed...");
          }

          @Override
          public void onNext(String s) {
            Log.d(TAG_TEST, "Next item: " + s);
          }

          @Override
          public void onError(Throwable e) {
            Log.e(TAG_TEST, "mapExample() - An error occurred: " + e.getMessage());
          }

          @Override
          public void onComplete() {
            Log.d(TAG_TEST, "mapExample() - Completed!");
          }
        });
  }

  private void filterExample() {
    RxDataManager.getInstance().filterExampleObservable()
        .subscribe(new Observer<Integer>() {
          @Override
          public void onSubscribe(Disposable d) {
            disposable = d;
            Log.d(TAG_TEST, "filterExample() - subscribed...");
          }

          @Override
          public void onNext(Integer s) {
            Log.d(TAG_TEST, "Next item: " + s);
          }

          @Override
          public void onError(Throwable e) {
            Log.e(TAG_TEST, "mapExample() - An error occurred: " + e.getMessage());
          }

          @Override
          public void onComplete() {
            Log.d(TAG_TEST, "filterExample() - Completed!");
          }
        });
  }

  private void zipExample() {
    RxDataManager.getInstance().zipExampleObserver()
        .subscribe(new Observer<List<User>>() {
          @Override
          public void onSubscribe(@NonNull Disposable d) {
            disposable = d;
            Log.d(TAG_TEST, "zipExample() - subscribed...");
          }

          @Override
          public void onNext(@NonNull List<User> users) {
            Log.d(TAG_TEST, "Users who love football and handball: ");
            for (User user : users) {
              Log.d(TAG_TEST, user.toString());
            }
          }

          @Override
          public void onError(@NonNull Throwable e) {
            Log.e(TAG_TEST, "zipExample() - An error occurred: " + e.getMessage());
          }

          @Override
          public void onComplete() {
            Log.d(TAG_TEST, "zipExample() - Completed!");
          }
        });
  }
}