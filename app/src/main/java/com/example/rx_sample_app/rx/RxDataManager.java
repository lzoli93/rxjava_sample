package com.example.rx_sample_app.rx;

import android.util.Log;
import com.example.rx_sample_app.data.UserDataHandler;
import com.example.rx_sample_app.model.User;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Author:  Zoltan Lorinczi
 * Date:    12/7/2020
 */
public class RxDataManager {

  private static final String TAG = RxDataManager.class.getSimpleName();

  private UserDataHandler userDataHandler;

  private static RxDataManager instance;

  private RxDataManager() {
    userDataHandler = new UserDataHandler();
  }

  public static RxDataManager getInstance() {
    if (instance == null) {
      instance = new RxDataManager();
    }
    return instance;
  }

  //
  // OBSERVABLES
  //

  public Observable<Integer> justExampleObservable() {
    return Observable.just(1, 2, 3, 4, 5, 6, 7, 8, 9);
  }

  public Observable<User> createExampleObservable() {
    return Observable.create(new ObservableOnSubscribe<User>() {
      @Override
      public void subscribe(@NonNull ObservableEmitter<User> emitter) throws Exception {
        for (User user : userDataHandler.getUsers()) {
          emitter.onNext(user);
        }
        emitter.onComplete();
      }
    });
  }

  //
  // SINGLE
  //

  public Single<String> singleExample() {
    return Single.just("This is a Single example.");
  }

  //
  // MAYBE
  //

  public Maybe<String> maybeExample() {
    return Maybe.just("This is a Maybe example.");
  }

  public Maybe<String> emptyMaybeExample() {
    return Maybe.empty();
  }

  //
  // THREADING EXAMPLES
  //

  public Observable<Integer> threadingExampleMixedThreads() {
    return Observable.just("long", "longer", "longest")
        .subscribeOn(Schedulers.io())
        .map(s -> {
          Log.d(TAG, " map() element: '" + s + "', on thread: " + Thread.currentThread().getName());
          return s.length();
        })
        .observeOn(Schedulers.computation())
        .filter(integer -> {
          Log.d(TAG, " filter() element: " + integer + ", on thread: " + Thread.currentThread().getName());
          return integer > 6;
        });
  }

  public Observable<Integer> threadingExampleMainThread() {
    return Observable.just("long", "longer", "longest")
        .map(s -> {
          Log.d(TAG, " map() element: '" + s + "', on thread: " + Thread.currentThread().getName());
          return s.length();
        })
        .filter(integer -> {
          Log.d(TAG, " filter() element: " + integer + ", on thread: " + Thread.currentThread().getName());
          return integer > 6;
        });
  }

  //
  // HOT AND COLD OBSERVABLES
  //

  public Observable<Long> coldObservable() {
    return Observable.interval(500, TimeUnit.MILLISECONDS);
  }

  public ReplaySubject<Integer> hotObservable() {
    return ReplaySubject.create();
  }

  //
  // OPERATORS
  //

  public Observable<String> mapExampleObservable() {
    return justExampleObservable().map(createStringFromIntFunction());
  }

  public Observable<Integer> filterExampleObservable() {
    return justExampleObservable().filter(new Predicate<Integer>() {
      @Override
      public boolean test(@NonNull Integer integer) throws Exception {
        return integer % 2 == 0;
      }
    });
  }

  public Observable<List<User>> zipExampleObserver() {
    return Observable.zip(
        footballFanUsersObservable(),
        handballFanUsersObservable(),
        new BiFunction<List<User>, List<User>, List<User>>() {
          @NonNull
          @Override
          public List<User> apply(@NonNull List<User> footballFans, @NonNull List<User> handballFans) throws Exception {
            List<User> userWhoLoveBoth = new ArrayList<>();
            for (User footballFan : footballFans) {
              if (handballFans.contains(footballFan)) {
                userWhoLoveBoth.add(footballFan);
              }
            }
            return userWhoLoveBoth;
          }
        });
  }

  public Observable<List<User>> footballFanUsersObservable() {
    return Observable.create(new ObservableOnSubscribe<List<User>>() {
      @Override
      public void subscribe(@NonNull ObservableEmitter<List<User>> emitter) throws Exception {
        ArrayList<User> footballFans = new ArrayList<>();
        for (User user : userDataHandler.getUsers()) {
          if (user.loveFootball()) {
            footballFans.add(user);
          }
        }
        emitter.onNext(footballFans);
        emitter.onComplete();
      }
    });
  }

  public Observable<List<User>> handballFanUsersObservable() {
    return Observable.create(new ObservableOnSubscribe<List<User>>() {
      @Override
      public void subscribe(@NonNull ObservableEmitter<List<User>> emitter) throws Exception {
        ArrayList<User> handballFans = new ArrayList<>();
        for (User user : userDataHandler.getUsers()) {
          if (user.loveHandball()) {
            handballFans.add(user);
          }
        }
        emitter.onNext(handballFans);
        emitter.onComplete();
      }
    });
  }

  //
  // Rx FUNCTIONS
  //

  private Function<Integer, String> createStringFromIntFunction() {
    return new Function<Integer, String>() {
      @Override
      public String apply(@NonNull Integer integer) {
        return String.valueOf(integer);
      }
    };
  }
}
