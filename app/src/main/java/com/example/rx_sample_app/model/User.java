package com.example.rx_sample_app.model;

/**
 * Author:  Zoltan Lorinczi
 * Date:    12/8/2020
 */
public class User {

  private int id;
  private String firstName;
  private String lastName;
  private int age;
  boolean loveFootball;
  boolean loveHandball;

  public User(int id, String firstName, String lastName, int age, boolean loveFootball, boolean loveHandball) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.loveFootball = loveFootball;
    this.loveHandball = loveHandball;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public boolean loveFootball() {
    return loveFootball;
  }

  public void setLoveFootball(boolean loveFootball) {
    this.loveFootball = loveFootball;
  }

  public boolean loveHandball() {
    return loveHandball;
  }

  public void setLoveHandball(boolean loveHandball) {
    this.loveHandball = loveHandball;
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", age=" + age +
        ", loveFootball=" + loveFootball +
        ", loveHandball=" + loveHandball +
        '}';
  }
}
